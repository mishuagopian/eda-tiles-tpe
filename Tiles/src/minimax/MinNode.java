package minimax;

import game.Board;
import game.Play;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class MinNode extends Node {

	public MinNode(Board board,Play play){
		super(board,play,Integer.MAX_VALUE);
	}
	
	
	@Override
	public void toTree(BufferedWriter file) {
		String aux=String.valueOf(this.hashCode());
		try {
			file.write(aux+" [shape=\"oval\"]");
			file.newLine();
			file.write(aux+" [label=\""+play+" ");
		} catch (IOException e) {
			e.printStackTrace();
		}
		super.toTree(file);
	}
	
	@Override
	public void makeChildren() {
		List<Play> plays=board.validPlays();
		children=new ArrayList<Node>();
		for(Play p: plays){
			children.add(new MaxNode(board.cloneBoard(),p));
		}
	}
	
	@Override
	public void checkIfBetter(Node aux) {
		if(aux==null){
			return;
		}
		if(aux.heuristic<heuristic){
			heuristic=(aux.heuristic);
			bestChild=aux;
			beta=aux.heuristic;
		}
	}
	
	@Override
	public boolean hasToPrune() {
		return (heuristic<=alpha);
	}
	
}

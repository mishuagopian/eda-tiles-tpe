package minimax;

import game.Board;
import game.Play;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MaxNode extends Node {

	public MaxNode(Board board, Play play) {
		super(board, play, Integer.MIN_VALUE);
	}

	@Override
	public void toTree(BufferedWriter file) {
		String aux = String.valueOf(this.hashCode());

		try {
			file.write(aux + " [shape=box]");
			file.newLine();
			if (play == null) {
				file.write(aux + " [label=\"START ");
			} else {
				file.write(aux + " [label=\"" + play + " ");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		super.toTree(file);
	}

	@Override
	public void checkIfBetter(Node aux) {
		if(aux==null){
			return;
		}
		if (aux.heuristic > heuristic) {
			heuristic = (aux.heuristic);
			bestChild = aux;
			alpha = aux.heuristic;
		}
	}

	@Override
	public void makeChildren() {
		List<Play> plays = board.validPlays();
		children = new ArrayList<Node>();
		for (Play p : plays) {
			children.add(new MinNode(board.cloneBoard(), p));
		}
	}

	@Override
	public boolean hasToPrune() {
		return (heuristic >= beta);
	}

}

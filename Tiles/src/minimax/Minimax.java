package minimax;

import game.Board;
import game.Play;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class Minimax {
	private boolean prune;
	private boolean depth;
	private boolean makeTree;
	private int quantity;
	private Node firstState;
	private long time;

	public Minimax(boolean depth, int quantity, boolean prune, boolean makeTree) {
		this.prune = prune;
		this.depth = depth;
		this.quantity = quantity;
		this.makeTree = makeTree;
		this.time = 0;
	}

	public Play bestPlayFound(Board board) {
		Play p;
		if (board.gameOver()) {
			return null;
		}
		if (depth) {
			firstState = calculatePlays(new MaxNode(board.cloneBoard(), null),
					quantity, Integer.MIN_VALUE, Integer.MAX_VALUE);
		} else {
			time = System.currentTimeMillis();
			int level = 1;
			while (!hasTimedOut()) {
				Node aux;
				aux = calculatePlays(new MaxNode(board.cloneBoard(), null),
						level, Integer.MIN_VALUE, Integer.MAX_VALUE);
				if (aux != null) {
					firstState = aux;
					if (firstState.getHeuristic() >= Integer.MAX_VALUE / 2
							|| firstState.getHeuristic() <= Integer.MIN_VALUE / 2) {
						break;
					}
				}
				level++;
			}

		}
		if (makeTree) {
			makeTree();
		}
		p = firstState.getBestChildPlay();
		firstState = null;
		return p;
	}

	private Node calculatePlays(Node node, int level, int alpha, int beta) {
		if (hasTimedOut()) {
			return null;
		}
		node.process();
		node.setAlpha(alpha);
		node.setBeta(beta);
		if (level == 0 || node.gameOver()) {
			node.leafHeuristic();
			return node;
		}
		node.makeChildren();
		node.board=null;
		for (Node n : node.getChildren()) {
			Node aux;
			if (hasTimedOut()) {
				return null;
			}
			aux = calculatePlays(n, level - 1, node.getAlpha(), node.getBeta());
			node.checkIfBetter(aux);
			if (prune && node.hasToPrune()) {
				break;
			}
		}
		if(!makeTree){
			node.children=null;
		}
		return node;
	}

	private void makeTree() {
		File f = new File("tree.dot");
		FileWriter fr = null;
		try {
			fr = new FileWriter(f);
			BufferedWriter br = new BufferedWriter(fr);
			br.write("graph tree {");
			br.newLine();
			firstState.toTree(br);
			br.write(String.valueOf(firstState.hashCode())
					+ " [fillcolor=\"pink\" style=\"filled\"]");
			br.newLine();
			br.write("}");
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean hasTimedOut() {
		return !depth
				&& ((System.currentTimeMillis() - time) > (quantity * 1000));
	}

}

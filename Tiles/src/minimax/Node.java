package minimax;

import game.Board;
import game.Play;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

public abstract class Node {

	protected Board board;
	protected List<Node> children;
	protected Node bestChild;
	protected Play play;
	protected int heuristic;
	private boolean visited;
	protected int alpha;
	protected int beta;

	public Node(Board board, Play play, int heuristic) {
		this.heuristic = heuristic;
		this.play = play;
		this.board = board;
		visited = false;
	}

	public void toTree(BufferedWriter file) {
		String aux = String.valueOf(this.hashCode());

		try {
			if (!visited) {
				file.write("\"]");
				file.newLine();
				file.write(aux + " [fillcolor=\"grey\" style=\"filled\"]");
				file.newLine();
				return;
			}
			if (heuristic >= Integer.MAX_VALUE/2) {
				file.write("WON " +String.valueOf(heuristic-Integer.MAX_VALUE/2)+"\"]");
			} else if (heuristic <= Integer.MIN_VALUE/2) {
				file.write("LOSS " +String.valueOf(heuristic-Integer.MIN_VALUE/2)+"\"]");
			} else {
				file.write(heuristic + "\"]");
			}
			file.newLine();
			if (children != null) {
				for (Node child : children) {
					String aux2 = String.valueOf(child.hashCode());
					file.write(aux + " -- " + aux2);
					file.newLine();
					child.toTree(file);
				}
				if (bestChild != null) {
					String aux2 = String.valueOf(bestChild.hashCode());
					file.write(aux2 + " [fillcolor=\"pink\" style=\"filled\"]");
					file.newLine();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void leafHeuristic() {
		if (board.gameOver()) {
			if (board.getScore1() < board.getScore2()) {
				heuristic = Integer.MIN_VALUE/2+board.getScore1()-board.getScore2();
			} else if (board.getScore1() > board.getScore2()) {
				heuristic = Integer.MAX_VALUE/2+board.getScore1()-board.getScore2();
			} else {
				heuristic = 0;
			}
		} else {
			heuristic = board.getScore1() - board.getScore2();
		}
	}

	public abstract void makeChildren();

	public void process() {
		if (play != null) {
			board.makePlay(play);
		}
		visited = true;
	}

	public boolean gameOver() {
		return board.gameOver();
	}

	public Play getBestChildPlay() {  
		if (bestChild == null) {
			return null;
		}
		return bestChild.play;
	}

	public abstract void checkIfBetter(Node aux);

	public List<Node> getChildren() {
		return children;
	}

	public abstract boolean hasToPrune();

	public int getAlpha() {
		return alpha;
	}

	public void setAlpha(int alpha) {
		this.alpha = alpha;
	}

	public int getBeta() {
		return beta;
	}

	public void setBeta(int beta) {
		this.beta = beta;
	}

	public int getHeuristic() {
		return heuristic;
	}
	
}

package game;

public class Play {

	private int row;
	private int col;
	
	public Play(int row, int col){
		this.row=row;
		this.col=col;
	}
	
	public String toString(){
		return "("+row+","+col+")";
	}
	
	public int getCol() {
		return col;
	}
	
	public int getRow() {
		return row;
	}
	
}

package game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Board {

	private int dimCols, dimRows, score1, score2;
	private char[][] board;
	private boolean p1Turn;

	public Board(String filename, boolean p1Turn) throws IOException {
		int i, j;
		File f = new File(filename);
		FileReader fr = null;
		fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		String line = null;
		line = br.readLine();
		dimRows = Integer.valueOf(line);
		line = br.readLine();
		dimCols = Integer.valueOf(line);
		line = br.readLine();
		score1 = Integer.valueOf(line);
		line = br.readLine();
		score2 = Integer.valueOf(line);
		if(!p1Turn){
			int aux;
			aux = score1;
			score1 = score2;
			score2 = aux;
		}
		if(dimRows<1 || dimCols<1 || dimRows>50 || dimCols>50){			// El tablero tiene dimension maxima de 50*50
			throw new IllegalArgumentException("Tamaño de la matriz invalido");
		}
		if(score1<0 || score2<0){
			throw new IllegalArgumentException("No se admiten puntajes negativos");
		}
		board=new char[dimCols][dimRows];
		for (i = 0; i < dimRows; i++) {
			char[] aux = new char[dimCols];
			line = br.readLine();
			if(line==null){
				throw new IllegalArgumentException("No coincide la cantidad de filas con el numero ingresado");
			}
			aux = line.toCharArray();
			if(aux.length!=dimCols){
				throw new IllegalArgumentException("No coincide la cantidad de columnas con el numero ingresado");
			}
			for (j = 0; j < dimCols; j++) {
				if (aux[j] == ' ') {
					board[j][i] = 0;
				} else if(aux[j]>='1' && aux[j]<='9' ) {
					board[j][i] = aux[j];
				}
				else{
					throw new IllegalArgumentException("Caracter invalido en la matriz");
				}
			}
		}
		line=br.readLine();
		if(line!=null){
			throw new IllegalArgumentException("Hay mas datos de los necesarios en el archivo");
		}
		br.close();
		arrangeTiles();
		this.p1Turn=p1Turn;
	}

	public Board() {
	}

	public Board(int rows, int cols, int colors){			// Para pruebas con tableros random
		dimRows=rows;
		dimCols=cols;
		p1Turn=true;
		score1=0;
		score2=0;
		board = new char[dimCols][dimRows];
		for (int j = 0; j < dimRows; j++) {
			for (int i = 0; i < dimCols; i++) {
				board[i][j]=(char)(Math.random()*colors+49);
			}
		}
	}
	
	public void printBoard() {	
		int i, j;
		for (j = 0; j < dimRows; j++) {
			for (i = 0; i < dimCols; i++) {
				System.out.print((int) (board[i][j])-48 + " ");
			}
			System.out.println();
		}
	}

	public boolean isValid(Play p) {
		int col = p.getCol(), row = p.getRow();
		if (col < 0 || col >= dimCols || row < 0 || row >= dimRows) {
			return false;
		}
		if (board[col][row] == 0) {
			return false;
		}
		if (row != 0 && board[col][row] == board[col][row - 1]) {
			return true;
		}
		if (row != dimRows - 1 && board[col][row] == board[col][row + 1]) {
			return true;
		}
		if (col != 0 && board[col][row] == board[col - 1][row]) {
			return true;
		}
		if (col != dimCols - 1 && board[col][row] == board[col + 1][row]) {
			return true;
		}
		return false;
	}

	public boolean gameOver() {
		int i, j;
		if (board[0][dimRows - 1] == 0) {
			return true;
		}
		for (i = 0; i < dimCols && board[i][dimRows - 1] != 0; i++) {
			for (j = dimRows - 1; j >= 0 && board[i][j] != 0; j--) {
				if (isValid(new Play(j, i))) {
					return false;
				}
			}
		}
		return true;
	}

	public int breakTile(Play p) {
		int col = p.getCol(), row = p.getRow();
		return breakTileRec(row, col, board[col][row]);
	}

	private int breakTileRec(int row, int col, char colour) {
		if (col < 0 || col >= dimCols || row < 0 || row >= dimRows) {
			return 0;
		}
		if (board[col][row] != colour) {
			return 0;
		}
		board[col][row] = 0;
		return 1 + breakTileRec(row - 1, col, colour)
				+ breakTileRec(row + 1, col, colour)
				+ breakTileRec(row, col - 1, colour)
				+ breakTileRec(row, col + 1, colour);
	}

	public void arrangeTiles() {
		int i;
		for (i = 0; i < dimCols; i++) {
			gravity(i);
		}
		arrangeCols();
	}

	private void gravity(int col) {
		int i, j;
		char aux;
		for (i = dimRows - 1, j = dimRows - 1; i >= 0; i--) {
			if (board[col][i] != 0) {
				aux = board[col][i];
				board[col][i] = board[col][j];
				board[col][j] = aux;
				j--;
			}
		}
	}

	private void arrangeCols() {
		int i, j;
		char[] aux;
		for (i = 0, j = 0; i < dimCols; i++) {
			if (board[i][dimRows - 1] != 0) {
				aux = board[i];
				board[i] = board[j];
				board[j] = aux;
				j++;
			}
		}
	}

	public Board cloneBoard() {
		Board b = new Board();
		b.dimCols = this.dimCols;
		b.dimRows = this.dimRows;
		b.board=new char[dimCols][dimRows];
		b.score1 = this.score1;
		b.score2 = this.score2;
		b.p1Turn = this.p1Turn;
		for (int i = 0; i < dimCols; i++) {
			b.board[i] = this.board[i].clone();
		}
		return b;
	}

	public List<Play> validPlays() {
		List<Play> plays = new ArrayList<Play>();
		Board b = cloneBoard();
		int i, j;
		for (i = 0; i < dimCols; i++) {
			for (j = 0; j < dimRows; j++) {
				Play p = new Play(j, i);
				if (b.isValid(p)) {
					b.breakTile(p);
					plays.add(p);
				}
			}
		}
		return plays;
	}
	
	public boolean makePlay(Play p){
		int tiles;
		if(p==null || !isValid(p)){
			return false;
		}
		tiles=breakTile(p);
		arrangeTiles();
		assignScore(tiles);
		p1Turn=!p1Turn;
		return true;
	}
	
	private void assignScore(int tiles){
		int score;
		boolean cleanBoard=(board[0][dimRows-1]==0);
		if(tiles<=5){
			score=(int)Math.pow(2,tiles-2);
		}
		else{
			score=2*tiles;
		}
		if(p1Turn){
			score1+=score;
			if(cleanBoard){
				score1*=1.3;
			}
		}
		else{
			score2+=score;
			if(cleanBoard){
				score2*=1.3;
			}
		}		
	}
	
	public String whoWon(){
		if(gameOver()){
			if(score1>score2){
				return "Computer Won!";
			}
			else if(score1<score2){
				return "Human Won!";
			}
			return "It was a tie!";
		}
		return "Game is not over yet.";
	}

	public int getScore1() {
		return score1;
	}

	public int getScore2() {
		return score2;
	}

	public int getDimCols() {
		return dimCols;
	}
	
	public int getDimRows() {
		return dimRows;
	}
	
	public char[][] getBoard() {
		return board;
	}
	
	public boolean isP1Turn() {
		return p1Turn;
	}


}

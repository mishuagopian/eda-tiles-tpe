package main;

import game.Board;
import game.Play;

import java.io.File;
import java.io.IOException;

import minimax.Minimax;
import visual.GameFrame;

public class Main {

	public static void main(String[] args) throws InterruptedException, IOException {
		Arguments arguments = new Arguments(args);
		Board board = new Board(arguments.filename, arguments.console);
		Minimax mM = new Minimax(arguments.depth, arguments.quantity, arguments.prune, arguments.makeTree);
		if (arguments.console) {
			Play p = mM.bestPlayFound(board);
			if (p == null) {
				System.out.println("No hay jugada");
			} else {
				System.out.println(p);
			}
		} else {
			new GameFrame(board, mM);
		}
	}

	private static class Arguments {
		private boolean depth;
		private boolean console;
		private boolean prune = false;
		private boolean makeTree = false;
		private int quantity;
		private String filename;

		public Arguments(String[] args) {
			this.validateArgs(args);
		}

		private void validateArgs(String[] args) {
			File file;
			int aux;
			aux = args.length;
			if (aux > 7 || aux < 5) {
				throw new IllegalArgumentException(
						"La cantidad de parametros ingresados es incorrecta");
			}
			if (!args[0].equals("-file")) {
				throw new IllegalArgumentException(
						"El primer parametro debe ser '-file'");
			}
			filename = args[1];
			file = new File(filename);
			if (!file.isFile()) {
				throw new IllegalArgumentException(
						"No se encontro el archivo: " + args[1]);
			}
			if (args[2].equals("-maxtime")) {
				depth = false;
			} else if (args[2].equals("-depth")) {
				depth = true;
			} else {
				throw new IllegalArgumentException(
						"El tercer parametro debe ser '-depth' o '-maxtime'");
			}
			quantity = Integer.valueOf(args[3]);
			if (quantity < 1) {
				throw new IllegalArgumentException(
						"El cuarto parametro debe ser un numero natural");
			}
			if (args[4].equals("-visual")) {
				console = false;
			} else if (args[4].equals("-console")) {
				console = true;
			} else {
				throw new IllegalArgumentException(
						"El quinto parametro debe ser '-visual' o '-console'");
			}
			if (aux > 5) {
				if (args[5].equals("-prune")) {
					prune = true;
					if (aux > 6) {
						if (args[6].equals("-tree")) {
							makeTree = true;
						} else {
							throw new IllegalArgumentException(
									"El septimo parametro solo puede ser '-tree'");
						}
					}
				} else if (args[5].equals("-tree")) {
					makeTree = true;
					if (aux != 6) {
						throw new IllegalArgumentException("Mal los parametros");
					}
				} else {
					throw new IllegalArgumentException("Mal los parametros");
				}

			}
			if (makeTree && !console) {
				throw new IllegalArgumentException(
						"No se puede hacer un arbol si no se pide jugar con '-console'");
			}
		}

	}

}

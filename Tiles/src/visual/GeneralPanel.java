package visual;

import game.Board;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;

import minimax.Minimax;



public class GeneralPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private int width=1000;
	private int height=600;
	private Minimax mM;
	
	
	public GeneralPanel(Board board, Minimax mM) {
		this.mM = mM;
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(width, height));
		MatrixPanel mp = new MatrixPanel(board, this, (int)(width*0.80),height);
		mp.setPreferredSize(new Dimension((int)(width*0.80), height));
		InfoPanel ip = new InfoPanel(board);
		ip.setPreferredSize(new Dimension((int)(width*0.20), height));
		this.add(mp, BorderLayout.EAST);
		this.add(ip, BorderLayout.WEST);
	}
	
	public Minimax getmM() {
		return mM;
	}

	
}

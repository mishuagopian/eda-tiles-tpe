package visual;

import game.Board;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.JPanel;




public class InfoPanel extends JPanel {


	private static final long serialVersionUID = 1L;
	private Board board;

	public InfoPanel(Board board) {
		this.board = board;
		this.setLayout(null);
		this.setBackground(Color.white);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		int centrado = (int)(this.getWidth()*0.28);
		Image img =  Toolkit.getDefaultToolkit().getImage(new File("").getAbsolutePath()+"/src/visual/Tiles.png");	
		g.drawImage(img, 0, 0, this);
		g.setFont(new Font("Comic Sans MS", Font.PLAIN, 18));
		g.setColor(Color.blue);
		g.drawString("PUNTAJES\n\n", centrado, this.getHeight()/4+50);
		g.drawString("Player:  "+board.getScore2()+"\n\n", centrado, this.getHeight()/4+100);
		g.drawString("Com:  "+board.getScore1()+"\n\n", centrado, this.getHeight()/4+150);
		if(board.gameOver()){
			g.drawString(board.whoWon(), centrado, this.getHeight()/4+250);
		}
		else{
			if(board.isP1Turn()){
				g.drawString("Com playing", centrado, this.getHeight()/4+250);				
			}
			else{
				g.drawString("Your turn", centrado, this.getHeight()/4+250);
			}
		}
	}	
	
}
